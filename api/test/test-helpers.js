import User from '../models/user';
import Story from '../models/story';
import Comment from '../models/comment';
import SocialAuth from '../models/socialAuth';
import RBAC from '../utils/rbac';

export function getCookies(user) {
  return [`composedream_AUTH_TOKEN=${user.token}`];
}

export async function roleReset(user, roles) {
  const acl = RBAC.getAcl();
  const deleteRoles = ['admin', 'user', 'blocked'];

  await acl.asyncRemoveUserRoles(user.id, deleteRoles);
  await acl.asyncAddUserRoles(user.id, roles);
}

export async function dbReset() {
  try {
    await User.remove({});
    await Story.remove({});
    await SocialAuth.remove({});
    await Comment.remove({});
    return true;
  } catch (err) {
    return false;
  }
}
