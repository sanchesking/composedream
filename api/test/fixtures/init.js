import mongoose from 'mongoose';
import Monky from 'monky';
const monky = new Monky(mongoose);

monky.factory({ name: 'SocialAuthAdmin', model: 'SocialAuth' }, {
  uid:   '#n',
  token: '#nEAAF3ZC6Ki4BABAJRwC7kJZBhZAPDHVBamRwnQGSOkRGQOJwabZBOI81ndMhZA6VWd8gyJlV6lanNk7tuyRs1Y5A63ihDKzczlHMpZB0K1oiaoL5tU8aHCrEM7BDSlOSKOJSAWcIHXrfPjW8NOZAT3wuLNJIbAhbOXavYQCaerRWT1oCCZAIscKqqZCHvP6kCZB5rcZD',
  type:  'fb',
  user:  monky.ref('Admin', '_id'),
  data:  {}
});

monky.factory({ name: 'SocialAuthUser', model: 'SocialAuth' }, {
  uid:   '#n',
  token: '#nEAAF3ZC6Ki4BABACarSxhMk3pggptvZC7mw99ItDyNuFXRJcNZCm4N8gRYmxf4dguH5j7sGhQ72Fb95mm1sImDgZBRp2BlP4ud7GbJuQkceJ5VPXMK12pkfFYkt7MYqiuYz1f9XHtQerF14KIvPuwyVrHuBdZCw7UIe4rR07RHjTs2k5GvZCyc6InKgYom50tQZD',
  type:  'fb',
  user:  monky.ref('User', '_id'),
  data:  {}
});

monky.factory({ name: 'Admin', model: 'User' }, {
  name:     '#n name',
  email:    'composedream.test@gmail.com',
  story:    monky.ref('Story', '_id'),
  location: 'UA',
  phone:    '123456789',
  status:   'active',
  about:    'about',
  hit:      1,
  incHit:   1
});

monky.factory({ name: 'User', model: 'User' }, {
  name:     '#n name',
  avatar:   '/avatar/base64',
  email:    'user_#n@gmail.com',
  story:    monky.ref('Story', '_id'),
  location: 'UA',
  about:    'about',
  phone:    '123456789',
  status:   'active',
  hit:      1,
  incHit:   1
});

monky.factory('Story', {
  title:    'title story #n',
  story:    'text story #n',
  likes:    [],
  incLikes: 1
});

monky.factory('Comment', {
  entityName: 'profile',
  entityId:   monky.ref('User', '_id'),
  author:     monky.ref('User', '_id'),
  text:       'comment text #n',
  type:       'public',
  viewed:     false
});

monky.factory('UserActivity', {
  user:   monky.ref('User', '_id'),
  action: 'online',
});

module.exports.monky = monky;
