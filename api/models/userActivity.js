import mongoose from 'mongoose';
import config from 'config';
const Schema = mongoose.Schema;

const ONLINE_TIMEOUT = 1000 * 60 * config.userActivity.noActivityMinutes;

const userActivitySchema = new Schema({
  user:      { type: Schema.Types.ObjectId, ref: 'User' },
  action:    { type: 'String', enum: ['online', 'offline'] },
  expiredAt: { type: 'Date' },
});

userActivitySchema.statics.findOneStatusByUser = function(query, cb) {
  return this.findOne({ user: query, expiredAt: { $gt: Date.now() }}, cb)
    .or([{ action: 'online' }, { action: 'offline' }]);
};

userActivitySchema.statics.findStatusByUser = function(query, cb) {
  return this.find({ user: query, expiredAt: { $gt: Date.now() } }, cb)
    .or([{ action: 'online' }, { action: 'offline' }]);
};

userActivitySchema.statics.findOneAndUpdateStatusByUser = function(userId, action) {
  return this.findOneAndUpdate(
    { user: userId, $or: [{ action: 'online' }, { action: 'offline' }] },
    { action: action, expiredAt: Date.now() + ONLINE_TIMEOUT },
    { upsert: true }
  )
};

export default mongoose.model('UserActivity', userActivitySchema);
