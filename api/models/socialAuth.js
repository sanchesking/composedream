import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const socialAuthSchema = new Schema({
  user:      { type: Schema.Types.ObjectId, ref: 'User' },
  token:     { type: 'String' },
  type:      { type: 'String', enum: ['fb'], default: 'fb' },
  uid:       { type: 'String' },
  data:      { type: 'Object', default: {} },
  createdAt: { type: 'Date', default: Date.now },
  updatedAt: { type: 'Date' }
});

export default mongoose.model('SocialAuth', socialAuthSchema);
