import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const commentSchema = new Schema({
  author:    { type: Schema.Types.ObjectId, ref: 'User' },
  text:      { type: 'String' },
  createdAt: { type: 'Date', default: Date.now }
});

const storySchema = new Schema({
  author:          { type: Schema.Types.ObjectId, ref: 'User' },
  title:           { type: 'String' },
  story:           { type: 'String' },
  likes:           [{ type: Schema.Types.ObjectId, ref: 'User' }],
  totalLikes:      { type: 'Number', default: 0 },
  incLikes:        { type: 'Number', default: 0 },
  likesUpdateDate: { type: 'Number', default: 0 },
  comments:        [commentSchema],
  createDate:      { type: 'Date', default: Date.now, required: false }
});

storySchema.set('toObject', {
  virtuals:  true,
  transform: function(doc, ret, options) {
    delete ret.likes;
    if (!options.isAdmin) {
      delete ret.incLikes;
    }
    return ret;
  }
});

storySchema.virtual('likesCount').get(function() {
  if (this.likes) {
    return parseInt(this.incLikes, 10) + parseInt(this.likes.length, 10);
  }
  return parseInt(this.incLikes, 10);
});


export default mongoose.model('Story', storySchema);
