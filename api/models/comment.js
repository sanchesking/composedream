import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const commentSchema = new Schema({
  entityName: { type: 'String', enum: ['profile'], default: 'profile' },
  entityId:   { type: Schema.Types.ObjectId },
  author:     { type: Schema.Types.ObjectId, ref: 'User' },
  text:       { type: 'String' },
  parent:     { type: Schema.Types.ObjectId, ref: 'Comment', default: null },
  type:       { type: 'String', enum: ['private', 'public'], default: 'public' },
  viewed:     { type: 'Boolean', default: false },
  likes:      [{ type: Schema.Types.ObjectId, ref: 'User' }],
  createdAt:  { type: 'Date', default: Date.now }
});

export default mongoose.model('Comment', commentSchema);
