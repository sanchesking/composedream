import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const categorySchema = new Schema({
  parent: { type: Schema.Types.ObjectId, ref: 'Category' },
  name:   { type: 'String' }
});

categorySchema.set('toObject', {
  virtuals: true
});

categorySchema.virtual('children', {
  ref:          'Category', // The model to use
  localField:   '_id', // Find people where `localField`
  foreignField: 'parent' // is equal to `foreignField`
});

categorySchema.statics.initCategory = async (Category) => {
  if (await Category.count() > 0) return;

  const parent = await Category.create({ parent: null, name: 'businessCategories' });
  const _categories = [
      { parent: parent._id, name: 'АВТОБИЗНЕС' },
      { parent: parent._id, name: 'БЫТОВЫЕ УСЛУГИ' },
      { parent: parent._id, name: 'ЗДОРОВЬЕ' },
      { parent: parent._id, name: 'ЗООБИЗНЕС' },
      { parent: parent._id, name: 'ИНФОРМАЦИОННЫЕ ТЕХНОЛОГИИ И ЭЛЕКТРОТЕХНИКА' },
      { parent: parent._id, name: 'КРАСОТА' },
      { parent: parent._id, name: 'ОБРАЗОВАНИЕ И ИСКУССТВО' },
      { parent: parent._id, name: 'ОБСЛУЖИВАНИЕ БИЗНЕСА' },
      { parent: parent._id, name: 'ОБЩЕСТВЕННОЕ ПИТАНИЕ' },
      { parent: parent._id, name: 'ОДЕЖДА И АКСЕССУАРЫ' },
      { parent: parent._id, name: 'ПРОДУКТЫ И НАПИТКИ' },
      { parent: parent._id, name: 'ПРОМЫШЛЕННОЕ ПРОИЗВОДСТВО' },
      { parent: parent._id, name: 'РАЗВЛЕЧЕНИЯ' },
      { parent: parent._id, name: 'РИТЕЙЛ' },
      { parent: parent._id, name: 'СЕЛЬСКОЕ, ЛЕСНОЕ ХОЗЯЙСТВО И РЫБОЛОВСТВО' },
      { parent: parent._id, name: 'СМИ, ПОЛИГРАФИЯ И ИЗДАТЕЛЬСКИЙ БИЗНЕС' },
      { parent: parent._id, name: 'СПОРТ' },
      { parent: parent._id, name: 'СТРОИТЕЛЬСТВО И РЕМОНТ' },
      { parent: parent._id, name: 'ТОВАРЫ ДЛЯ ДЕТЕЙ' },
      { parent: parent._id, name: 'ТОВАРЫ ДЛЯ ДОМА' },
      { parent: parent._id, name: 'ТУРИСТИЧЕСКИЙ И ГОСТИНИЧНЫЙ БИЗНЕС' },
      { parent: parent._id, name: 'УСЛУГИ ДЛЯ ДЕТЕЙ' },
      { parent: parent._id, name: 'ФИНАНСОВЫЕ УСЛУГИ' },
      { parent: parent._id, name: 'ХОББИ-БИЗНЕС (ДОМАШНИЙ БИЗНЕС)' }
  ];

  _categories.forEach(category => {
    Category.create(category);
  });
};

export default mongoose.model('Category', categorySchema);
