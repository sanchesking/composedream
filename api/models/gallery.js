import mongoose from 'mongoose';
const Schema = mongoose.Schema;

// const itemSchema = new Schema({
//   imageUrl: { type: 'String' },
//   title:    { type: 'String' },
// });

const gallerySchema = new Schema({
  owner: { type: Schema.Types.ObjectId, ref: 'User' },
  // items: [itemSchema]
}, { strict: false });

export default mongoose.model('Gallery', gallerySchema);
