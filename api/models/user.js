import mongoose from 'mongoose';
import { getImageUrl } from '../utils/file';
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name:          { type: 'String', required: true },
  email:         { type: 'String', unique: true, required: true },
  phone:         { type: 'String', required: true },
  location:      { type: 'String', required: true },
  tagline:       { type: 'String', required: false },
  education:     { type: 'String', required: false },
  avatar:        { type: 'String', required: false },
  about:         { type: 'String', required: false },
  hidePhone:     { type: 'Boolean', default: false },
  hideEmail:     { type: 'Boolean', default: false },
  status:        { type: 'String', enum: ['inactive', 'active', 'blocked'], default: 'active' },
  capital:       { type: 'String', enum: ['private', 'corporate'], default: 'private' },
  type:          { type: 'Number', default: 0 },
  hit:           { type: 'Number', default: 0 },
  incHit:        { type: 'Number', default: 0 },
  confirmToken:  { type: 'String', select: false, default: '' },
  confirmExpire: { type: 'Number', select: false },
  winner:        { type: 'Boolean', default: false },
  story:         { type: Schema.Types.ObjectId, ref: 'Story' },
  friends:       [{ type: Schema.Types.ObjectId, ref: 'User' }],
  gallery_id:    { type: Schema.Types.ObjectId, ref: 'Gallery' },
  categories:    [{ type: Schema.Types.ObjectId, ref: 'Category' }],
  createDate:    { type: 'Date', default: Date.now, required: false },
});

userSchema.set('toObject', {
  virtuals:  true,
  transform: function(doc, ret, options) {
    if (!(options.isAdmin || (options.currentUser && options.currentUser._id === ret.id))) {
      ret.hideEmail && delete ret.email;
      ret.hidePhone && delete ret.phone;
    }
    if (!options.isAdmin) {
      delete ret.hit;
      delete ret.incHit;
    }
    delete ret.avatar;
    return ret;
  }
});

userSchema.virtual('avatarUrl').get(function() {
  return getImageUrl(this.avatar);
});

userSchema.virtual('hitCount').get(function() {
  if (this.hit !== undefined && this.incHit !== undefined) {
    return this.hit + this.incHit;
  }
});

export default mongoose.model('User', userSchema, 'user');
