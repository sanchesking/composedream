import config from 'config';
import FB from 'fb';
import User from '../models/user';
import SocialAuth from '../models/socialAuth';
import { fbApiAsync } from '../utils/fb';
import { validateRequest } from '../utils/validate';
import { formatErrors } from '../utils/errors';
import { generateToken } from '../utils/password';
import RBAC, { initUserRoles } from '../utils/rbac';
import { sendConfirmationLink } from '../utils/mailer';
import { convertToSessionUser } from '../utils/session';

async function fbRegistration(req, data) {
  req.checkBody('user.accessToken').notEmpty();
  req.checkBody('user.name').notEmpty();
  req.checkBody('user.email').isEmail();
  req.checkBody('user.phone').notEmpty();
  req.checkBody('user.location').notEmpty();
  req.checkBody('user.categories').optional().isArray();
  req.checkBody('user.capital').optional().isIn(['private', 'corporate']);

  let result = null;

  try {
    await validateRequest(req);

    FB.setAccessToken(data.accessToken);

    const fbMe = await fbApiAsync('/me?fields=email');

    if (!fbMe || fbMe.error) {
      req.res.status(500);
      return !fbMe ? 'error occurred' : fbMe.error;
    }

    const userExist = await SocialAuth.findOne({ uid: fbMe.id }).populate('user', '_id');
    if (userExist) {
      req.res.status(500);
      return 'Email already exists';
    }

    const userByMail = {
      token:      data.accessToken,
      email:      fbMe.email,
      name:       data.name,
      phone:      data.phone,
      location:   data.location,
      type:       data.userType,
      categories: data.categories,
      capital:    data.capital,
      status:     'active'
    };

    const userByPhone = {
      token:         data.accessToken,
      email:         data.email,
      name:          data.name,
      phone:         data.phone,
      confirmToken:  generateToken(),
      confirmExpire: Date.now() + 3600 * 24 * 1000,
      location:      data.location,
      type:          data.userType,
      categories:    data.categories,
      capital:       data.capital,
      status:        'inactive'
    };

    let localUser = new User(Boolean(fbMe.email) ? userByMail : userByPhone);
    localUser = await localUser.save();

    let socialAuth = new SocialAuth({
      uid:   fbMe.id,
      token: data.accessToken,
      type:  'fb',
      user:  localUser._id,
      data:  fbMe
    });
    socialAuth = await socialAuth.save();

    localUser = localUser.toObject({ virtuals: true });
    await initUserRoles(localUser.id, fbMe.email);

    localUser.roles = await RBAC.getAcl().asyncUserRoles(localUser.id);
    req.session.user = convertToSessionUser(localUser, localUser.roles);

    if (localUser.status === 'inactive') {
      sendConfirmationLink(config.mail.admin, localUser.email, localUser);
    }

    delete localUser.confirmToken;
    delete localUser.confirmExpire;

    result = localUser;
    result.token = socialAuth.token;
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}

export default async function reg(req) {
  const type = req.body.type;
  const data = type ? req.body.user : req.body;

  let result = null;

  if (type === 'fb') {
    result = await fbRegistration(req, data);
  }

  return result;
}
