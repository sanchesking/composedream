import { convertToSessionUser } from '../utils/session';
import Q from 'q';
import FB from 'fb';
import RBAC from '../utils/rbac';
import { SocialAuth, UserActivity } from '../models/index';

export default function login(req) {
  const acl = RBAC.getAcl();
  const type = req.body.type;
  const data = type ? req.body.user : req.body;
  const deferred = Q.defer();

  if (type === 'fb') {
    FB.setAccessToken(data.accessToken);
    FB.api('/me?fields=email', async function(res) {

      if (!res || res.error) {
        console.log(!res ? 'error occurred' : res.error);
        deferred.reject(res.error);
      }

      try {
        let auth = await SocialAuth.findOne({ uid: res.id }).populate('user', 'name email avatar status capital type hit incHit');
        if (!auth) {
          deferred.resolve({ redirect: 'registration' });
          return;
        }

        auth.token = data.accessToken;
        auth = await auth.save();

        const userId = auth.user.id;
        const roles = await acl.asyncUserRoles(userId);
        req.session.user = convertToSessionUser(auth.user, roles);

        const result = auth.user.toObject();
        result.token = auth.token;
        result.roles = roles;

        UserActivity.findOneAndUpdateStatusByUser(userId, 'online').exec();

        deferred.resolve(result);
      } catch (err) {
        deferred.reject(err.message);
      }
    });
  } else {
    deferred.reject('Incorrect type registration');
  }

  return deferred.promise;
}
