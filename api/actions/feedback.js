import { validateRequest } from '../utils/validate';
import { formatErrors } from '../utils/errors';
import mailer from '../utils/mailer';
import config from 'config';

export default async function feedback(req) {
  req.checkBody('senderName').notEmpty();
  req.checkBody('senderEmail').isEmail();
  req.checkBody('purpose').notEmpty();
  req.checkBody('message').notEmpty();
  req.sanitizeBody('name').escape();
  req.sanitizeBody('purpose').escape();
  req.sanitizeBody('message').escape();

  let result = { errors: 'Can not send feedback' };

  try {
    await validateRequest(req);

    const params = req.body;

    const from = params.senderName;
    const to = 'feron6332@gmail.com';
    const subject = params.purpose;
    const tplName = 'feedback';
    const locals = {
      title:       'Feedback',
      senderName:  params.senderName,
      senderEmail: params.senderEmail,
      message:     params.message
    };
    await mailer.init(config.mailer);
    await mailer.sendMailTemplate(from, to, subject, tplName, locals);

    result = { success: true };
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
