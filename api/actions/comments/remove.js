import Comment from '../../models/comment';
import { formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';

async function remove(req) {
  req.checkBody('commentId').isMongoId();
  req.checkBody('userId').optional().isMongoId();

  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');

  let result = { errors: ['Can not delete comment'] };

  try {
    await validateRequest(req);
    const { commentId, userId } = req.body;

    const authorId = currentUserIsAdmin && userId ? userId : currentUser._id;

    const isDeleted = await Comment.find().or([{ _id: commentId, author: authorId }, { parent: commentId }]).remove();

    result = { success: Boolean(isDeleted.result.ok) };
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}

export default remove;
