import Comment from '../../models/comment';
import { formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';

export default async function view(req) {
  req.checkBody('commentIds').isArray();

  const currentUser = req.session.user;

  let result = { success: false };

  try {
    await validateRequest(req);
    const { commentIds } = req.body;

    await Comment.update({ _id: { $in: commentIds } }, { viewed: true }, { new: true, multi: true });
    const viewed = await Comment.find({ _id: { $in: commentIds } }, 'viewed');

    result = viewed.map(itm => itm.toObject());
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
