import { formatErrors, HttpError } from '../../utils/errors';
import Comment from '../../models/comment';
import { validateRequest } from '../../utils/validate';

export default async function update(req) {
  req.checkBody('commentId').isMongoId();
  req.checkBody('userId').optional().isMongoId();
  req.checkBody('comment').notEmpty();

  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');

  let result = { errors: 'Can not update comment' };

  try {
    await validateRequest(req);
    const { commentId, userId, comment: text } = req.body;

    const authorId = currentUserIsAdmin && userId ? userId : currentUser._id;

    let comment = await Comment.findOne({ _id: commentId, author: authorId }).populate('author', 'name email avatar hit incHit');
    if (!comment) throw new HttpError(404);

    comment.text = text;
    comment = await comment.save();

    result = comment.toObject();
  } catch (err) {
    return formatErrors(req, err);
  }
  return result;
}
