import { formatErrors, HttpError } from '../../utils/errors';
import { absoluteClientUrl } from '../../utils/url';
import User from '../../models/user';
import Comment from '../../models/comment';
import { validateRequest } from '../../utils/validate';
import mailer from '../../utils/mailer';
import config from 'config';

/**
 * Send notification from author comment to author profile
 * @param from
 * @param to
 * @returns {Promise.<*>}
 */
export async function sendCommentedNotification(from, to, comment) {
  const subject = comment.parent ? 'Commented your comment' : 'Commented your profile';
  const tplName = 'comment';
  const locals = {
    authorLink: absoluteClientUrl('/profile/' + from.id),
    author:     from,
    comment
  };
  await mailer.init(config.mailer);
  return await mailer.sendMailTemplate(from.email, to.email, subject, tplName, locals);
}

export default async function create(req) {
  req.checkQuery('entityId').isMongoId();
  req.checkQuery('entityName').isIn(['profile']);
  req.checkBody('parent').optional().isMongoId();
  req.checkBody('type').optional().isIn(['private', 'public']);
  req.checkBody('comment').notEmpty();

  const currentUserId = req.session.user._id;
  let result = { errors: 'Can not update profile' };

  try {
    await validateRequest(req);

    const { entityId, entityName } = req.query;
    const { comment: text, parent, type } = req.body;

    const recipientEntity = await User.findById(entityId);
    if (!recipientEntity) throw new HttpError(404);

    let comment = new Comment();
    comment.entityName = entityName;
    comment.entityId = recipientEntity._id;
    comment.author = currentUserId;
    comment.text = text;
    comment.parent = parent;
    if (!parent) comment.viewed = true;
    if (!parent && type) comment.type = type;
    comment = await comment.save();

    result = (await Comment.populate(comment, { path: 'author', select: 'name email avatar hit incHit' })).toObject();

    if (parent) {
      const parentComment = await Comment.findById(parent).populate('author', 'name email');
      sendCommentedNotification(comment.author, parentComment.author, result);
    } else {
      sendCommentedNotification(comment.author, recipientEntity, result);
    }
  } catch (err) {
    return formatErrors(req, err);
  }
  return result;
}
