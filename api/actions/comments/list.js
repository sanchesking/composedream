import Comment from '../../models/comment';
import { validateRequest } from '../../utils/validate';
import { formatErrors } from '../../utils/errors';
import _ from 'underscore';

function commentsTree(comments, children) {
  const childrenHash = children.reduce((carry, child) => {
    if (!carry[child.parent.toString()]) {
      carry[child.parent] = [];
    }

    carry[child.parent].push(child);
    return carry;
  }, {});
  return comments.map(parent => {
    parent.children = childrenHash[parent.id];
    return parent;
  });
}

export default async function list(req) {
  req.checkQuery('entityId').isMongoId();
  req.checkQuery('entityName').isIn(['profile']);

  let result = null;

  const currentUser = req.session.user;
  const currentUserIsAdmin = currentUser ? currentUser.isAdmin : false;

  try {
    await validateRequest(req);
    const { entityName, entityId } = req.query;


    const or = [{ type: { $eq: 'public' } }];
    if (currentUser) {
      if (currentUserIsAdmin) {
        or.push({ type: { $eq: 'private' } });
      } else {
        or.push({ type: { $eq: 'private' }, entityId: { $eq: currentUser._id } });
        or.push({ type: { $eq: 'private' }, author: { $eq: currentUser._id } });
      }
    }

    const comments = await Comment
      .find({ parent: { $eq: null }, entityId, entityName })
      .or(or)
      .sort({ createdAt: 1 })
      .populate('author', 'name avatar hit incHit');
    const commentsId = comments.map(comment => comment._id);
    const children = await Comment.find({ parent: { $in: commentsId } }).sort({ createdAt: 1 }).populate('author', 'name avatar hit incHit');

    const parentComments = comments.map(comment => comment.toObject({ virtuals: true, isAdmin: currentUserIsAdmin }));
    const childrenComments = children.map(comment => comment.toObject({ virtuals: true, isAdmin: currentUserIsAdmin }));

    result = commentsTree(parentComments, childrenComments);
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
