import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import Comment from '../../models/comment';

export default async function like(req) {
  req.checkBody('commentId').isMongoId();

  const { commentId } = req.body;
  const currentUserId = req.session.user._id;

  let result = null;

  try {
    await validateRequest(req);

    const data = await Comment.findById(commentId).populate('author', 'status');

    if (!data) throw new HttpError(404, 'Story not found');
    if (data.author.id === currentUserId) throw new HttpError(400, 'Can not like yourself comment');
    if (data.author.status === 'blocked' ) throw new HttpError(400, 'Author of the comment is blocked');

    const userLike = await Comment.findOne({ _id: data._id, likes: { $elemMatch: { $eq: currentUserId } } });

    if (userLike) {
      data.likes.pull(currentUserId);
    } else {
      data.likes.push(currentUserId);
    }

    result = await data.save();
    result = { likesCount: result.likes.length };
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
