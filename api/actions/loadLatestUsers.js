import User from '../models/user';

export default async function getLatestUsers(req) {
  let result = {};
  const limit = req.body.limit || 3;
  const currentUser = req.session.user;
  const currentUserIsAdmin = currentUser ? ~(currentUser.roles || []).indexOf('admin') : 0;

  try {
    const data = await User.find({ status: { $eq: 'active' } })
      .limit(limit)
      .sort({ createDate: -1 });

    result = data.map(item => {
      const obj = item.toObject();
      obj.story.likesCount = obj.story.likes.length + obj.story.incLikes;
      if (!currentUserIsAdmin) {
        delete obj.story.likes;
        delete obj.story.incLikes;
      }
      return obj;
    });
  } catch (err) {
    req.res.status(500);
    return err;
  }

  return result;
}
