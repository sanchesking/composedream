import _ from 'underscore';
import { User, UserActivity } from '../models/index';

async function attachActivity(users) {
  const status = await UserActivity.findStatusByUser({ $in: users.map(itm => itm.id) });
  const statusHash = _.object(_.pluck(status, 'user'), _.pluck(status, 'action'));

  return users.map(user => {
    const userId = user._id.toString();
    user.activity = statusHash[userId] ? statusHash[userId] : 'offline';
    return user;
  });
}

export default async function loadMainPage(req) {
  const page = {};

  try {
    page.dreamers = await User.where('type').equals(0).count();
    page.startups = await User.where('type').equals(1).count();
    page.investors = await User.where('type').equals(2).count();

    const lastRegs = await User.find(
      {},
      'name avatar location story hit incHit status'
    )
      .limit(10)
      .sort({ createDate: -1 })
      .populate('story', 'likes incLikes totalLikes title');

    page.lastRegisters = lastRegs.map(item => {
      return item.toObject();
    });
    page.lastRegisters = await attachActivity(page.lastRegisters);

    const winners = await User.find({ 'winner': true }, 'name avatar location story hit incHit status').limit(3)
      .sort({ createDate: -1 })
      .populate('story', 'likes incLikes totalLikes story title');

    page.winners = winners.map(item => {
      const obj = item.toObject();
      return obj;
    });
    page.winners = await attachActivity(page.winners);
  } catch (err) {
    console.log(err);

    req.res.status(500);
    return err;
  }

  return page;
}
