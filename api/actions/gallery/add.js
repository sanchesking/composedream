import { ObjectId as objectId } from 'mongodb';
import Gallery from '../../models/gallery';
import { writeFile, getImageUrl } from '../../utils/file';
import { HttpError, formatErrors } from '../../utils/errors';
import asyncBusboy from 'async-busboy';

async function add(req) {
  let result = null;

  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');

  try {
    // console.log('0===', user);
    const formData = await asyncBusboy(req);
    const userId = currentUserIsAdmin && formData.fields.userId ? formData.fields.userId : currentUser._id;
    // console.log('1===', user);
    let gallery = await Gallery.findOne({ owner: userId });
    if (!gallery) {
      throw new HttpError(404, 'Gallery not found');
    }
    const galleryItemId = objectId().toString();
    // console.log('2===');
    const imageUrl = await writeFile(formData.files[0], `/gallery/${gallery._id}`, galleryItemId);
    const galleryItem = { imageUrl, title: formData.fields.title };
    // console.log('3===');
    gallery = await Gallery.findOneAndUpdate(
      { _id: gallery._id },
      { $set: { [galleryItemId]: galleryItem } },
      { new: true }
    );
    gallery = gallery.toObject();
    // console.log('4===');

    result = gallery[galleryItemId];
    if (result.imageUrl) {
      result.imageUrl = getImageUrl(result.imageUrl);
      result._id = galleryItemId;
    }
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}

export default add;
