import Gallery from '../../models/gallery';
import { validateRequest } from '../../utils/validate';
import { formatErrors } from '../../utils/errors';

export default async function remove(req) {
  req.checkBody('userId').optional().isMongoId();

  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');

  const userId = currentUserIsAdmin && req.body.userId ? req.body.userId : currentUser._id;
  const galleryItemId = req.body.id;
  const galleryItemTitle = req.body.title;

  let result = false;
  try {
    await validateRequest(req);

    const updatedFieldName = `${galleryItemId}.title`;
    const gallery = await Gallery.findOneAndUpdate(
      { owner: userId },
      { $set: {
        [updatedFieldName]: galleryItemTitle
      } },
      { new: true, projection: { [galleryItemId]: 1 } }
    );

    result = gallery[galleryItemId];
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
