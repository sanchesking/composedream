import { ObjectId as objectId } from 'mongodb';
import Gallery from '../../models/gallery';
import { deleteFile } from '../../utils/file';
import { formatErrors } from '../../utils/errors';

async function remove(req) {
  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');
  const userId = currentUserIsAdmin && req.body.userId ? req.body.userId : currentUser._id;
  const removeId = req.body.id;

  let result = { errors: ['Can not delete file'] };
  try {
    const galleryItem = (await Gallery.findOne(
      { owner: objectId(userId), [removeId]: { $exists: true } },
      { [removeId]: 1 }
    )).toObject();

    if (galleryItem) {
      let isDeleted = await deleteFile(galleryItem[removeId].imageUrl);
      isDeleted = isDeleted && await Gallery.update({ 'owner': userId }, { $unset: { [removeId]: 1 } });

      if (isDeleted) {
        result = { success: true };
      }
    }
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}

export default remove;
