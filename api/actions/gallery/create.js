import { ObjectId as objectId } from 'mongodb';
import Gallery from '../../models/gallery';
import User from '../../models/user';
import { validateRequest } from '../../utils/validate';
import { formatErrors } from '../../utils/errors';

export default async function create(req) {
  req.checkBody('userId').optional().isMongoId();

  let result = null;
  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');
  const userId = currentUserIsAdmin && req.body.userId ? req.body.userId : currentUser._id;

  try {
    await validateRequest(req);

    const gallery = await Gallery.findOneAndUpdate(
      { owner: userId },
      { $set: { owner: objectId(userId) } },
      { upsert: true, new: true }
    );

    await User.findOneAndUpdate(
      { _id: userId },
      { $set: { gallery_id: gallery._id } },
      { upsert: true, new: true }
    );

    result = gallery._id;
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
