import _ from 'underscore';
import Gallery from '../../models/gallery';
import { getImageUrl } from '../../utils/file';
import { formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';

export default async function read(req) {
  req.checkQuery('id').isMongoId();

  let result = false;

  try {
    await validateRequest(req);

    const galleryId = req.query.id;
    const gallery = await Gallery.findOne({ _id: galleryId }, '-_id -owner -__v');

    result = _.map(gallery.toObject(), (item, key) => {
      item._id = key;
      item.imageUrl = getImageUrl(item.imageUrl);
      return item;
    });
  } catch (err) {
    return formatErrors(req, err);
  }
  return result;
}
