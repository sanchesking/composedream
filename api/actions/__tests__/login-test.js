import chai from 'chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import app from '../../api';
import User from '../../models/user';
import SocialAuth from '../../models/socialAuth';
import Story from '../../models/story';
import RBAC from '../../utils/rbac';
import FB from 'fb';
import { monky } from '../../test/fixtures/init';

chai.should();
chai.use(chaiHttp);

describe('login', function() {
  beforeEach(function(done) {
    const acl = RBAC.getAcl();
    const suite = this;
    FB.api = sinon.stub();
    FB.api.withArgs('/me?fields=email').onCall(0).yields({ id: '116602705545714' });

    User.remove({}, async() => {
      await SocialAuth.remove({});

      suite.user = await monky.create('User');
      suite.socialAuth = await monky.create('SocialAuth', { uid: '116602705545714', user: suite.user._id });

      const deleteRoles = ['admin', 'user', 'blocked'];

      await acl.asyncRemoveUserRoles(suite.user.id, deleteRoles);
      await acl.asyncAddUserRoles(suite.user.id, 'user');
      done();
    });
  });

  it('login from fb', function(done) {
    const suite = this;
    chai.request(app)
      .post('/login')
      .send({
        type: 'fb',
        user: {
          accessToken: 'EAACEdEose0cBAFojznP0id6iZBTDvfFx17tujPUA6hZBI7adzhb1phrfCWJTLL0m2l6Ko84ZAGhQmX29AZAV7M8XUx55pzEOZCBf8v1FgIdCRZC0paPuMI8ZCCEMQzYZBScWvXK3ASez5fT8Xy6vJnDpeZAel5xEbs3wmKMrm8wNhwRCHZCB97ZC5WqZCC8sQZAHNKToZD',
        }
      })
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

});
