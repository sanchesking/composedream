import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import app from '../../api';
import User from '../../models/user';
import Story from '../../models/story';
import RBAC from '../../utils/rbac';
import { monky } from '../../test/fixtures/init';
import { getCookies } from '../../test/test-helpers';
import { ObjectID as objectID } from 'mongodb';

chai.should();
chai.use(chaiHttp);

describe('main page', function() {
  beforeEach(function(done) {
    const acl = RBAC.getAcl();
    const suite = this;

    User.remove({}, async () => {
      await Story.remove({});

      suite.user = await monky.create('User', { status: 'active' });
      suite.userWithoutStory = await monky.create('User', { status: 'active', story: undefined });

      const deleteRoles = ['admin', 'user', 'blocked'];

      await acl.asyncRemoveUserRoles(suite.user.id, deleteRoles);
      await acl.asyncAddUserRoles(suite.user.id, 'user');

      await acl.asyncRemoveUserRoles(suite.userWithoutStory.id, deleteRoles);
      await acl.asyncAddUserRoles(suite.userWithoutStory.id, 'user');
      done();
    });
  });

  it('last registers', function(done) {
    chai.request(app)
      .get('/loadMainPage')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.lastRegisters[0].should.have.not.property('story');
        res.body.lastRegisters[1].should.have.property('story');
        done();
      });
  });

});
