import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import chaiThings from 'chai-things';
import timekeeper from 'timekeeper';
import User from '../../models/user';
import Story from '../../models/story';
import app from '../../api';
import { monky } from '../../test/fixtures/init';
import { getCookies, dbReset, roleReset } from '../../test/test-helpers';

chai.should();
chai.use(chaiHttp);
chai.use(chaiThings);

describe('userActivities', function() {
  beforeEach(async function() {
    const suite = this;

    await dbReset();
    suite.activity = await monky.create('UserActivity', { action: 'online', expiredAt: Date.now() + 60 * 1000 });
    suite.activity2 = await monky.create('UserActivity', { action: 'online', expiredAt: Date.now() + 60 * 1000 });
    suite.activity3 = await monky.create('UserActivity', { action: 'offline', expiredAt: Date.now() + 60 * 1000 });
  });

  it('online', function(done) {
    const suite = this;

    chai.request(app)
      .post('/userActivities/online')
      .send({ userIds: [ suite.activity.user, suite.activity2.user, suite.activity3.user ] })
      .end((err, res) => {
        res.should.have.status(200);
        res.body[suite.activity.user].should.eq('online');
        res.body[suite.activity2.user].should.eq('online');
        res.body[suite.activity3.user].should.eq('offline');
        done();
      });
  });

  it('expired activity', function(done) {
    const suite = this;
    const expired = Date.parse(suite.activity.expiredAt);
    timekeeper.travel(new Date(expired + 10 * 1000));

    chai.request(app)
      .post('/userActivities/online')
      .send({ userIds: [ suite.activity.user, suite.activity2.user, suite.activity3.user ] })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.to.be.empty;
        done();
      });
  });
});
