import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import chaiThings from 'chai-things';
import User from '../../models/user';
import Story from '../../models/story';
import app from '../../api';
import { monky } from '../../test/fixtures/init';
import { getCookies, dbReset, roleReset } from '../../test/test-helpers';

chai.should();
chai.use(chaiHttp);
chai.use(chaiThings);

describe('friends', function() {
  beforeEach(async function() {
    const suite = this;

    await dbReset();
    suite.user2 = await monky.create('User', {});
    suite.user3 = await monky.create('User', {});
    suite.user = await monky.create('User', { friends: [suite.user2._id] });

    await Story.findOneAndUpdate({ _id: suite.user2.story }, { $set: { author: suite.user2._id } });

    suite.auth = await monky.create('SocialAuthUser', { user: suite.user._id });
    suite.auth2 = await monky.create('SocialAuthUser', { user: suite.user2._id });
    suite.auth3 = await monky.create('SocialAuthUser', { user: suite.user3._id });

    await roleReset(suite.user, 'user');
    await roleReset(suite.user2, 'user');
    await roleReset(suite.user3, 'user');
  });

  it('create friend', function(done) {
    const suite = this;
    chai.request(app)
      .post('/friends/create')
      .send({ userId: suite.user3.id })
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.success.should.to.be.true;
        done();
      });
  });

  it('remove friend', function(done) {
    const suite = this;
    chai.request(app)
      .post('/friends/remove')
      .send({ userId: suite.user2.id })
      .set('Cookie', getCookies(suite.auth))
      .end(async (err, res) => {
        if (err) done(err);

        try {
          res.should.have.status(200);
          const user = await User.findOne({ friends: { $elemMatch: { $eq: suite.user2.id } } });
          expect(user).to.be.null;
          done();
        } catch (err2) {
          done(err2);
        }
      });
  });

  it('list friends', function(done) {
    const suite = this;

    chai.request(app)
      .get('/friends/list?userId=' + suite.user.id)
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.length.should.be.equal(1);
        res.body.should.include.something.that.has.property('avatarUrl');
        res.body.should.include.something.that.has.property('name');
        res.body.should.include.something.that.has.property('id');
        res.body.should.include.something.that.has.property('hitCount');
        res.body.should.include.something.that.have.deep.property('story.totalLikes');
        res.body.should.include.something.that.have.deep.property('story.likesCount');
        done();
      });
  });
});
