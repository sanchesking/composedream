import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import app from '../../api';
import User from '../../models/user';
import Story from '../../models/story';
import SocialAuth from '../../models/socialAuth';
import RBAC from '../../utils/rbac';
import { monky } from '../../test/fixtures/init';
import { getCookies } from '../../test/test-helpers';

chai.should();
chai.use(chaiHttp);

describe('feedback', function() {
  beforeEach(function(done) {
    const acl = RBAC.getAcl();
    const suite = this;

    User.remove({}, async () => {
      await Story.remove({});
      await SocialAuth.remove({});

      suite.user = await monky.create('User', { status: 'active' });
      suite.auth = await monky.create('SocialAuthUser', { user: suite.user._id });

      const deleteRoles = ['admin', 'user', 'blocked'];

      await acl.asyncRemoveUserRoles(suite.user.id, deleteRoles);
      await acl.asyncAddUserRoles(suite.user.id, 'user');
      done();
    });
  });

  it('feedback', function(done) {
    const suite = this;
    const params = {
      senderName:  'Test Feedback',
      senderEmail: 'test@test.com',
      purpose:     'purpose',
      message:     'test message'
    };
    chai.request(app)
      .post('/feedback')
      .send(params)
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.property('success');
        done();
      });
  });
});
