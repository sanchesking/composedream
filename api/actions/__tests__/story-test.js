import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import app from '../../api';
import User from '../../models/user';
import Story from '../../models/story';
import SocialAuth from '../../models/socialAuth';
import RBAC from '../../utils/rbac';
import { monky } from '../../test/fixtures/init';
import { getCookies } from '../../test/test-helpers';
import { ObjectID as objectID } from 'mongodb';

chai.should();
chai.use(chaiHttp);

describe('stories', function() {
  beforeEach(function(done) {
    const acl = RBAC.getAcl();
    const suite = this;

    User.remove({}, async () => {
      await Story.remove({});
      await SocialAuth.remove({});

      suite.user = await monky.create('User', { status: 'blocked', story: undefined });
      suite.user2 = await monky.create('User', {});

      suite.auth = await monky.create('SocialAuthUser', { user: suite.user._id });
      suite.auth2 = await monky.create('SocialAuthUser', { user: suite.user2._id });

      await Story.findOneAndUpdate({ _id: suite.user2.story }, { $set: { author: suite.user2._id } });

      const deleteRoles = ['admin', 'user', 'blocked'];

      await acl.asyncRemoveUserRoles(suite.user.id, deleteRoles);
      await acl.asyncAddUserRoles(suite.user.id, 'user');

      await acl.asyncRemoveUserRoles(suite.user2.id, deleteRoles);
      await acl.asyncAddUserRoles(suite.user2.id, 'user');
      done();
    });
  });

  it('create story', function(done) {
    const suite = this;
    expect(suite.user.story).to.be.an('undefined');
    chai.request(app)
      .post('/stories/create')
      .send({ userId: suite.user.id, story: { title: 'title 1', story: 'story 1' } })
      .set('Cookie', getCookies(suite.auth))
      .end(async (err, res) => {
        try {
          res.should.have.status(200);
          const author = await User.findById(res.body.author);
          author.story.should.to.eql(objectID(res.body._id));
          done();
        } catch (error) {
          done(error);
        }
      });
  });

  it('update story permission denied', function(done) {
    const suite = this;
    const params = {
      userId: suite.user2.id
    };
    chai.request(app)
      .post('/stories/update')
      .send(params)
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(403);
        done();
      });
  });
});
