import chai from 'chai';
import chaiHttp from 'chai-http';
import chaiThings from 'chai-things';
import app from '../../api';
import Comment from '../../models/comment';
import { monky } from '../../test/fixtures/init';
import { getCookies, dbReset, roleReset } from '../../test/test-helpers';
import { absoluteClientUrl } from '../../utils/url';
import { sendCommentedNotification } from '../comments/create';

chai.should();
chai.use(chaiHttp);
chai.use(chaiThings);

describe('comments', function() {
  beforeEach(async function() {
    const suite = this;

    await dbReset();
    suite.admin = await monky.create('Admin');
    suite.user = await monky.create('User');
    suite.user2 = await monky.create('User');
    suite.user3 = await monky.create('User');

    suite.authAdmin = await monky.create('SocialAuthAdmin', { user: suite.admin._id });
    suite.auth = await monky.create('SocialAuthUser', { user: suite.user._id });
    suite.auth2 = await monky.create('SocialAuthUser', { user: suite.user2._id });
    suite.auth3 = await monky.create('SocialAuthUser', { user: suite.user3._id });

    suite.comment = await monky.create('Comment', { entityId: suite.user2.id, author: suite.user.id });
    suite.childComment = await monky.create('Comment', { entityId: suite.user2.id, author: suite.user2.id, parent: suite.comment._id });
    suite.commentUserWithLike = await monky.create('Comment', { entityId: suite.user2.id, author: suite.user.id, likes: [suite.user2._id] });
    suite.privateComment = await monky.create('Comment', { entityId: suite.user2.id, author: suite.user.id, type: 'private' });

    await roleReset(suite.admin, 'admin');
    await roleReset(suite.user, 'user');
    await roleReset(suite.user2, 'user');
    await roleReset(suite.user3, 'user');
  });

  it('create comment', function(done) {
    const suite = this;
    chai.request(app)
      .post(`/comments/create?entityName=profile&entityId=${suite.user2.id}`)
      .send({ parent: suite.childComment.id, comment: 'comment text' })
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.author.should.has.property('name');
        res.body.author.should.has.property('email');
        res.body.author.should.has.property('avatarUrl');
        res.body.author.should.has.property('hitCount');
        res.body.author.should.not.has.property('incHit');
        res.body.author.should.not.has.property('hit');

        res.body.parent.should.equal(suite.childComment.id);
        done();
      });
  });

  it('create private comment', function(done) {
    const suite = this;
    chai.request(app)
      .post(`/comments/create?entityName=profile&entityId=${suite.user2.id}`)
      .send({ type: 'private', comment: 'comment text' })
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.author.should.has.property('name');
        res.body.author.should.has.property('email');
        res.body.author.should.has.property('avatarUrl');
        res.body.author.should.has.property('hitCount');
        res.body.author.should.not.has.property('incHit');
        res.body.author.should.not.has.property('hit');

        res.body.should.not.have.property('parent');
        done();
      });
  });

  it('update comment', function(done) {
    const suite = this;
    chai.request(app)
      .post('/comments/update')
      .send({ commentId: suite.comment.id, comment: 'new comment text' })
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.text.should.equal('new comment text');
        done();
      });
  });

  it('update ыomeone else comment', function(done) {
    const suite = this;
    chai.request(app)
      .post('/comments/update')
      .send({ commentId: suite.childComment.id, comment: 'new comment text' })
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });

  it('remove comment', function(done) {
    const suite = this;
    chai.request(app)
      .post('/comments/remove')
      .send({ commentId: suite.comment.id })
      .set('Cookie', getCookies(suite.auth))
      .end(async (err, res) => {
        try {
          res.should.have.status(200);
          const count = await Comment.count({ _id: { $in: [suite.comment.id, suite.childComment.id] } });
          count.should.equal(0);
          done();
        } catch (error) {
          done(error);
        }
      });
  });

  it('like comment', function(done) {
    const suite = this;
    chai.request(app)
      .post('/comments/like')
      .send({ commentId: suite.comment.id })
      .set('Cookie', getCookies(suite.auth2))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.likesCount.should.eql(1);
        done();
      });
  });

  it('like yourself comment', function(done) {
    const suite = this;
    chai.request(app)
      .post('/comments/like')
      .send({ commentId: suite.comment.id })
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(400);
        res.body.errors.should.eql('Can not like yourself comment');
        done();
      });
  });

  it('unlike comment', function(done) {
    const suite = this;
    chai.request(app)
      .post('/comments/like')
      .send({ commentId: suite.commentUserWithLike.id })
      .set('Cookie', getCookies(suite.auth2))
      .end((err, res) => {
        res.body.likesCount.should.eql(0);
        res.should.have.status(200);
        done();
      });
  });

  it('list comments as guest', function(done) {
    const suite = this;

    chai.request(app)
      .get(`/comments/list?entityName=profile&entityId=${suite.user2.id}`)
      .send({ comment: 'comment text' })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.length.should.equal(2);
        res.body[0].author.should.has.property('name');
        res.body[0].author.should.has.property('avatarUrl');
        res.body[0].author.should.has.property('hitCount');
        res.body[0].author.should.not.has.property('incHit');
        res.body[0].author.should.not.has.property('hit');

        res.body[0].children[0].author.should.has.property('name');
        res.body.should.not.include({ type: 'public' });
        done();
      });
  });

  it('list comments', function(done) {
    const suite = this;

    chai.request(app)
      .get(`/comments/list?entityName=profile&entityId=${suite.user2.id}`)
      .send({ comment: 'comment text' })
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.length.should.equal(3);
        res.body[0].author.should.has.property('name');
        res.body[0].author.should.has.property('avatarUrl');
        res.body[0].author.should.has.property('hitCount');
        res.body[0].author.should.not.has.property('incHit');
        res.body[0].author.should.not.has.property('hit');

        res.body[0].children[0].author.should.has.property('name');
        res.body.should.include.one({ type: 'private' });
        done();
      });
  });

  it('list comments as owner profile entity', function(done) {
    const suite = this;

    chai.request(app)
      .get(`/comments/list?entityName=profile&entityId=${suite.user2.id}`)
      .send({ comment: 'comment text' })
      .set('Cookie', getCookies(suite.auth2))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.length.should.equal(3);
        res.body[0].author.should.has.property('name');
        res.body[0].author.should.has.property('avatarUrl');
        res.body[0].author.should.has.property('hitCount');
        res.body[0].author.should.not.has.property('incHit');
        res.body[0].author.should.not.has.property('hit');

        res.body[0].children[0].author.should.has.property('name');
        res.body.should.include.one({ type: 'private' });
        done();
      });
  });

  it('list private comment from admin', function(done) {
    const suite = this;

    chai.request(app)
      .get(`/comments/list?entityName=profile&entityId=${suite.user2.id}`)
      .send({ comment: 'comment text' })
      .set('Cookie', getCookies(suite.authAdmin))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.contain.a.thing.with.property('type', 'private');
        done();
      });
  });

  it('send mail after comment', async function() {
    const from = {
      id:    '1234567890',
      email: 'fromEmail@gmail.com',
      name:  'Commenter'
    };
    const to = {
      email: 'toEmail@gmail.com'
    };
    const comment = { text: 'comment text' };

    const res = await sendCommentedNotification(from, to, comment);
    const message = res.response.toString();
    message.should.to.contain(`${absoluteClientUrl('profile/1234567890')}`);
    message.should.to.contain(`Subject: Commented your profile`);
    message.should.to.contain('comment text');
  });

  it('send mail after reply comment', async function() {
    const suite = this;
    const from = {
      id:    '1234567890',
      email: 'fromEmail@gmail.com',
      name:  'Commenter'
    };
    const to = {
      email: 'toEmail@gmail.com'
    };
    const comment = { text: 'comment text', parent: suite.comment.id };

    const res = await sendCommentedNotification(from, to, comment);
    const message = res.response.toString();
    message.should.to.contain(`${absoluteClientUrl('profile/1234567890')}`);
    message.should.to.contain(`Subject: Commented your comment`);
    message.should.to.contain('comment text');
  });

  it('view not viewed comment', function(done) {
    const suite = this;

    chai.request(app)
      .post('/comments/view')
      .send({ commentIds: [suite.comment.id] })
      .set('Cookie', getCookies(suite.auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.contain.a.thing.with.property('viewed', true);
        done();
      });
  });
});
