import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import chaiThings from 'chai-things';
import sinon from 'sinon';
import app from '../../api';
import User from '../../models/user';
import Story from '../../models/story';
import RBAC from '../../utils/rbac';
import FB from 'fb';
import { monky } from '../../test/fixtures/init';
import { dbReset, getCookies, roleReset } from '../../test/test-helpers';
import timekeeper from 'timekeeper';

chai.should();
chai.use(chaiHttp);

describe('users', function() {
  beforeEach(async function() {
    const acl = RBAC.getAcl();
    const suite = this;
    FB.api = sinon.stub();
    FB.api.withArgs('/me?fields=email').onCall(0).yields({ id: '116602705545714' });

    await dbReset();
    suite.admin = await monky.create('Admin');
    suite.user = await monky.create('User');
    suite.user2 = await monky.create('User');
    suite.blocked = await monky.create('User', { status: 'blocked', story: null });
    suite.hidePhoneEmail = await monky.create('User', { hidePhone: true, hideEmail: true });
    suite.userWithoutStory = await monky.create('User', { story: null });
    suite.inactive = await monky.create('User', { status: 'inactive', confirmToken: '111', confirmExpire: (Date.now() + 3600 * 24 * 1000) });

    suite.activityAdmin = await monky.create('UserActivity', { action: 'offline' });
    suite.activity = await monky.create('UserActivity', { action: 'online' });
    suite.activity2 = await monky.create('UserActivity', { action: 'online' });

    suite.adminAuth = await monky.create('SocialAuthAdmin', { user: suite.admin._id });
    suite.userAuth = await monky.create('SocialAuthUser', { user: suite.user._id });
    suite.user2Auth = await monky.create('SocialAuthUser', { user: suite.user2._id });

    suite.story = await Story.findById(suite.user.story);

    await roleReset(suite.admin, 'admin');
    await roleReset(suite.user, 'user');
    await roleReset(suite.user2, 'user');
    await roleReset(suite.hidePhoneEmail, 'user');
    await roleReset(suite.blocked, 'blocked');
    await roleReset(suite.inactive, 'inactive');
  });

  it('registration fb by phone', function(done) {
    const suite = this;
    chai.request(app)
      .post('/reg')
      .send({
        type: 'fb',
        user: {
          accessToken: 'EAACEdEose0cBAFojznP0id6iZBTDvfFx17tujPUA6hZBI7adzhb1phrfCWJTLL0m2l6Ko84ZAGhQmX29AZAV7M8XUx55pzEOZCBf8v1FgIdCRZC0paPuMI8ZCCEMQzYZBScWvXK3ASez5fT8Xy6vJnDpeZAel5xEbs3wmKMrm8wNhwRCHZCB97ZC5WqZCC8sQZAHNKToZD',
          location:    'UA',
          name:        'Валера Коровелков',
          phone:       '123456789',
          email:       'test@mail.com',
          userType:    0
        }
      })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.property('token');
        done();
      });
  });

  it('confirm email', function(done) {
    const acl = RBAC.getAcl();
    const suite = this;
    chai.request(app)
      .post('/users/confirm')
      .send({ token: '111' })
      .end(async (err, res) => {
        res.should.have.status(200);
        const roles = await acl.asyncUserRoles(suite.inactive.id);
        roles.should.include('user');
        roles.should.not.include('inactive');
        done();
      });
  });

  it('reset confirm email token', function(done) {
    const suite = this;
    chai.request(app)
      .post('/users/confirmReset')
      .send({ userId: suite.inactive.id })
      .end(async (err, res) => {
        res.should.have.status(200);
        const user = await User.findById(suite.inactive.id).select('+confirmToken');
        user.confirmToken.should.not.equal(suite.inactive.confirmToken);
        res.body.success.should.equal(true);
        done();
      });
  });

  it('top users from admin', function(done) {
    const suite = this;
    chai.request(app)
      .get('/users/top?type=1')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  it('top users from user', function(done) {
    const suite = this;
    chai.request(app)
      .get('/users/top?type=1')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });

  it('update hit user from guest', function(done) {
    const suite = this;
    chai.request(app)
      .post('/users/hit')
      .send({ userId: suite.user.id })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.property('hitCount');
        res.body.hitCount.should.be.eq(3);
        done();
      });
  });

  it('blocked user from admin', function(done) {
    const suite = this;
    chai.request(app)
      .post('/users/update')
      .set('Cookie', getCookies(suite.adminAuth))
      .send({ _id: suite.user.id, status: 'blocked' })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.status.should.eql('blocked');
        res.body.roles.should.eql(['blocked']);
        res.body.should.to.have.property('incHit');
        done();
      });
  });

  it('blocked user from user', function(done) {
    const suite = this;
    chai.request(app)
      .post('/users/update')
      .set('Cookie', getCookies(suite.userAuth))
      .send({ _id: suite.user2.id, status: 'blocked' })
      .end((err, res) => {
        res.should.have.status(403);
        done();
      });
  });

  it('blocked yourself', function(done) {
    const suite = this;
    chai.request(app)
      .post('/users/update')
      .set('Cookie', getCookies(suite.userAuth))
      .send({ _id: suite.user.id, status: 'blocked' })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.status.should.eql('active');
        res.body.roles.should.eql(['user']);
        res.body.should.to.have.property('hitCount');
        res.body.should.not.have.property('incHit');
        if (res.body.story) {
          res.body.story.should.to.have.property('likesCount');
          res.body.story.should.not.have.property('incLikes');
          res.body.story.should.not.have.property('likes');
        }
        done();
      });
  });

  it('winner user from admin', function(done) {
    const suite = this;
    suite.user.winner.should.equal(false);

    chai.request(app)
      .post('/users/update')
      .set('Cookie', getCookies(suite.adminAuth))
      .send({ _id: suite.user.id, winner: true })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.to.have.property('winner');
        res.body.winner.should.equal(true);
        done();
      });
  });

  it('winner user from not admin', function(done) {
    const suite = this;
    suite.user2.winner.should.equal(false);

    chai.request(app)
      .post('/users/update')
      .set('Cookie', getCookies(suite.userAuth))
      .send({ _id: suite.user2.id, winner: true })
      .end((err, res) => {
        res.should.have.status(403);
        res.body.should.to.have.property('errors');
        done();
      });
  });

  it('update user from yourself', function(done) {
    const suite = this;
    const now = suite.user.createDate;
    timekeeper.freeze(now);
    const nowISO = now.toISOString();

    const params = {
      _id:        suite.user.id,
      incHit:     1,
      email:      suite.user.email,
      winner:     false,
      status:     'active',
      categories: [],
      capital:    'private',
      name:       'a&aa',
      tagline:    'a&aa',
      phone:      'a&aa',
      location:   'a&aa',
      education:  'a&aa',
      about:      '<a&aa',
    };

    const result = {
      '__v':        0,
      '_id':        suite.user.id,
      'id':         suite.user.id,
      'avatarUrl':  'http://localhost:3000/api/avatar/base64',
      'capital':    'private',
      'categories': [],
      'createDate': nowISO,
      'friends':    [],
      'about':      '<a&aa',
      'education':  'a&aa',
      'location':   'a&aa',
      'name':       'a&aa',
      'phone':      'a&aa',
      'tagline':    'a&aa',
      'email':      suite.user.email,
      'hideEmail':  false,
      'hidePhone':  false,
      'hitCount':   2,
      'roles':      ['user'],
      'status':     'active',
      'type':       0,
      'winner':     false,
      'story':      {
        '_id':        suite.story.id,
        'id':         suite.story.id,
        'likesCount': 1,
        'story':      suite.story.story,
        'title':      suite.story.title,
      },
    };

    chai.request(app)
      .post('/users/update')
      .set('Cookie', getCookies(suite.userAuth))
      .send(params)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.eql(result);
        done();
      });
  });

  it('users list with sort by story.likes desc', function(done) {
    const suite = this;
    chai.request(app)
      .get('/users/list')
      .query({ fields: 'name,story', sort: 'story.likesCount_desc' })
      .end((err, res) => {
        res.should.have.status(200);
        res.body[0].should.have.property('avatarUrl');
        expect(res.body[0].story.likesCount >= res.body[1].story.likesCount).to.be.true;
        res.body.should.have.not.property('storyExist');
        res.body[8].should.have.not.property('story');
        res.body.length.should.be.equal(9);
        done();
      });
  });

  it('read user from undefined user', function(done) {
    const suite = this;
    chai.request(app)
      .get('/users/read')
      .query({ id: suite.user.id })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.contain.a.property('about', 'about');
        res.body.should.contain.a.property('avatarUrl', 'http://localhost:3000/api/avatar/base64');
        res.body.should.contain.a.property('name', suite.user.name);
        res.body.should.contain.a.property('activity', 'offline');
        res.body.should.contain.a.property('id', suite.user.id);
        res.body.should.contain.a.property('_id', suite.user.id);
        expect(Object.keys(res.body).length).to.equal(6);
        done();
      });
  });

  it('check hidden phone and email', function(done) {
    const suite = this;
    chai.request(app)
      .get('/users/read')
      .query({ id: suite.hidePhoneEmail.id })
      .set('Cookie', getCookies(suite.user2Auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.have.not.property('phone');
        res.body.should.have.property('hidePhone');
        res.body.should.have.not.property('email');
        res.body.should.have.property('hideEmail');
        done();
      });
  });

  it('hide email', function(done) {
    const suite = this;
    chai.request(app)
      .post('/users/hideEmailToggle')
      .set('Cookie', getCookies(suite.user2Auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.hideEmail.should.to.be.true;
        done();
      });
  });

  it('hide phone', function(done) {
    const suite = this;
    chai.request(app)
      .post('/users/hidePhoneToggle')
      .set('Cookie', getCookies(suite.user2Auth))
      .end((err, res) => {
        res.should.have.status(200);
        res.body.hidePhone.should.to.be.true;
        done();
      });
  });
});
