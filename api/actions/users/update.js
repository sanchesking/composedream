import { writeBase64File } from '../../utils/file';
import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import RBAC from '../../utils/rbac';
import User from '../../models/user';

async function updateRole(user, prevStatus) {
  const key = user._id.toString();
  const acl = RBAC.getAcl();

  if (prevStatus === 'active' && user.status === 'blocked') {
    await acl.asyncAddUserRoles(key, ['blocked']);
    await acl.asyncRemoveUserRoles(key, ['user']);
  } else if (prevStatus === 'blocked' && user.status === 'active') {
    await acl.asyncAddUserRoles(key, ['user']);
    await acl.asyncRemoveUserRoles(key, ['blocked']);
  }

  return await acl.asyncUserRoles(key);
}

export default async function update(req) {
  req.checkBody('_id').isMongoId();
  req.checkBody('incHit').optional().isInt();
  req.checkBody('email').optional().isEmail();
  req.checkBody('winner').optional().isBoolean();
  req.checkBody('status').optional().isIn(['active', 'blocked']);
  req.checkBody('categories').optional().isArray();
  req.checkBody('capital').optional().isIn(['private', 'corporate']);

  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');
  let result = { errors: 'Can not update profile' };

  try {
    await validateRequest(req);

    const profile = req.body;
    const userId = profile._id;
    const hasPermission = (currentUserIsAdmin || (currentUser._id === userId));

    if (!hasPermission) throw new HttpError(403);

    const user = await User.findById(userId);
    if (!user) throw new HttpError(404);

    const prevStatus = user.status;

    if (profile.name) user.name = profile.name;
    if (profile.phone) user.phone = profile.phone;
    if (profile.location) user.location = profile.location;
    if (profile.tagline) user.tagline = profile.tagline;
    if (profile.education) user.education = profile.education;
    if (profile.about) user.about = profile.about;
    if (currentUserIsAdmin && profile.winner !== undefined) user.winner = profile.winner;
    if (currentUserIsAdmin && profile.incHit) user.incHit = profile.incHit;
    if (currentUserIsAdmin && profile.email) user.email = profile.email;
    if (profile.avatar) user.avatar = await writeBase64File(profile.avatar, `/uimg/${userId}`, 'avatar');
    if (profile.categories) user.categories = profile.categories;
    if (currentUserIsAdmin && profile.status) user.status = profile.status;

    const data = await User.populate(await user.save(), { path: 'story', select: 'title story likes incLikes likesCount' });
    result = data.toObject({ virtuals: true, isAdmin: currentUserIsAdmin });
    result.roles = await updateRole(data, prevStatus);
  } catch (err) {
    return formatErrors(req, err);
  }
  return result;
}
