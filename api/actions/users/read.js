import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import { User, UserActivity } from '../../models/index';

export default async function read(req) {
  req.checkQuery('id', 'Invalid user id').isMongoId();

  let result = null;
  const currentUser = req.session.user;
  const currentUserIsAdmin = currentUser ? ~(currentUser.roles || []).indexOf('admin') : 0;

  try {
    await validateRequest(req);

    let data = User.findById(req.query.id)
    if (currentUser) {
      data
        .populate('categories', 'name')
        .populate('story', 'title story likes incLikes likesCount totalLikes');
    } else {
      data.select('name avatar about');
    }

    data = await data.exec();
    if (!data) throw new HttpError(404);

    const status = await UserActivity.findOneStatusByUser(req.query.id);

    result = data.toObject({ virtuals: true, currentUser, isAdmin: currentUserIsAdmin });
    result.activity = status ? status.action : 'offline';
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
