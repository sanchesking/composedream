import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import User from '../../models/user';
import { sendConfirmationLink } from '../../utils/mailer';
import { generateToken } from '../../utils/password';
import config from 'config';

export default async function confirmReset(req) {
  req.checkBody('userId').isMongoId();

  let result = { success: false };

  try {
    await validateRequest(req);

    const { userId } = req.body;

    let user = await User.findById(userId);
    if (!user) throw new HttpError(404);

    if (user.status === 'inactive') {
      user.confirmToken = generateToken();
      user.confirmExpire = Date.now() + 3600 * 24 * 1000;
      user = await user.save();
      await sendConfirmationLink(config.mail.admin, user.email, user);
      result = { success: true };
    }
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
