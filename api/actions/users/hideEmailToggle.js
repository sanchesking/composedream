import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import User from '../../models/user';

export default async function hideEmailToggle(req) {
  let result = null;

  try {
    const userId = req.session.user._id;

    const data = await User.findById(userId).select('hideEmail');
    if (!data) throw new HttpError(404);

    data.hideEmail = !data.hideEmail;
    result = await data.save();
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
