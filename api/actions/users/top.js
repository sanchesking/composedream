import _ from 'underscore';
import { validateRequest } from '../../utils/validate';
import { formatErrors } from '../../utils/errors';
import { Story, UserActivity } from '../../models/index';

async function attachActivity(stories) {
  const userIds = stories.map(itm => itm.author._id);
  const status = await UserActivity.findStatusByUser({ $in: userIds });
  const statusHash = _.object(_.pluck(status, 'user'), _.pluck(status, 'action'));

  return stories.map(story => {
    const userId = story.author.id;
    story.author.activity = statusHash[userId] ? statusHash[userId] : 'offline';
    return story;
  });
}

export default async function top(req) {
  req.checkQuery('type').optional().isIn([0, 1, 2]);
  req.checkQuery('limit').optional().isInt();

  let result = null;

  try {
    await validateRequest(req);

    const type = parseInt(req.query.type, 10);
    const limit = parseInt(req.query.limit, 10) || 1000;

    let match = { 'authorDoc._id': { $exists: true } };
    if (!isNaN(type)) {
      match = { 'authorDoc.type': { $eq: type } };
    }

    const story = await Story.aggregate()
      .lookup({
        from:         'user',
        localField:   'author',
        foreignField: '_id',
        as:           'authorDoc'
      })
      .match(match)
      .project({
        author:     1,
        // hitCount:   { $add: [ { $arrayElemAt: [ '$authorDoc.hit', 0 ] }, { $arrayElemAt: [ '$authorDoc.incHit', 0 ] } ] },
        likesCount: { $add: [ { $size: '$likes' }, '$incLikes' ] },
        totalLikes: '$totalLikes'
      })
      .sort('-likesCount')
      .limit(limit);

    result = await Story.populate(story, { path: 'author', select: 'name avatar hit incHit type location' });
    result = result.map(item => {
      item.author = item.author.toObject({ virtuals: true });
      return item;
    });

    if (!_.isEmpty(result)) {
      result = await attachActivity(result);
    }
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
