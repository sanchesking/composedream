import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import User from '../../models/user';

export default async function hidePhoneToggle(req) {
  let result = null;

  try {
    const userId = req.session.user._id;

    const data = await User.findById(userId).select('hidePhone');
    if (!data) throw new HttpError(404);

    data.hidePhone = !data.hidePhone;
    result = await data.save();
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
