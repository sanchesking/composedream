import _ from 'underscore';
import { getSafeFields } from '../../utils/db';
import { User, UserActivity } from '../../models/index';
import { formatErrors } from '../../utils/errors';
import { getImageUrl } from '../../utils/file';
import { validateRequest } from '../../utils/validate';

function safeFields() {
  return {
    _id:        true,
    name:       true,
    tagline:    true,
    phone:      true,
    location:   true,
    education:  true,
    avatar:     true,
    about:      true,
    status:     true,
    story:      true,
    hitCount:   true,
    createDate: true
  };
}

function getProjection(fields) {
  const safe = safeFields();
  if (~fields.indexOf('hitCount')) {
    safe.hit = true;
    safe.incHit = true;
    fields.push('hit');
    fields.push('incHit');
  }

  return getSafeFields(safe, fields);
}

function getSort(val) {
  const safe = Object.assign({}, safeFields(), {
    'story.likesCount': true
  });

  const defaultSort = ['createDate', 'desc'];
  const sort = val ? val.split('_') : defaultSort;
  sort[1] = sort[1] === 'desc' ? -1 : 1;

  return safe[sort[0]] ? sort : defaultSort;
}

async function attachActivity(users) {
  const status = await UserActivity.findStatusByUser({ $in: users.map(itm => itm._id) });
  const statusHash = _.object(_.pluck(status, 'user'), _.pluck(status, 'action'));

  return users.map(user => {
    const userId = user._id.toString();
    user.activity = statusHash[userId] ? statusHash[userId] : 'offline';
    return user;
  });
}

export default async function list(req) {
  req.checkQuery('fields').notEmpty();
  req.checkQuery('sort').optional().matches(/^[a-zA-Z0-9_.]+$/);
  req.checkQuery('limit').optional().isInt();

  let result = null;
  const currentUser = req.session.user;
  const currentUserIsAdmin = currentUser ? ~currentUser.roles.indexOf('admin') : false;

  try {
    await validateRequest(req);

    const fields = req.query.fields.split(',');
    const limit = parseInt(req.query.limit, 10) || 100;
    const sort = getSort(req.query.sort);
    fields.push(sort[0]);

    const query = {};
    const projection = getProjection(fields);

    if (!currentUserIsAdmin) {
      query.status = {
        $ne: 'blocked'
      };
    }

    if (projection.story) {
      delete projection.story;
      result = await User
        .aggregate()
        .lookup({
          from:         'stories',
          localField:   'story',
          foreignField: '_id',
          as:           'storyDoc'
        })
        .match(query)
        .project(Object.assign(projection, {
          'storyExist':       { $ifNull: [ '$story', false ] },
          'hitCount':         { $add: [ '$hit', '$incHit', 0 ] },
          'story.title':      { $arrayElemAt: [ '$storyDoc.title', 0 ] },
          'story.story':      { $arrayElemAt: [ '$storyDoc.story', 0 ] },
          'story.totalLikes': { $arrayElemAt: [ '$storyDoc.totalLikes', 0 ] },
          'story.likesCount': { $add: [
            { $size: { $ifNull: [{ $arrayElemAt: [ '$storyDoc.likes', 0 ] }, [] ] } },
            { $ifNull: [{ $arrayElemAt: [ '$storyDoc.incLikes', 0 ] }, 0 ] }
          ] }
        }))
        .sort({ [sort[0]]: sort[1] });

      result = result.map(item => {
        item.avatarUrl = getImageUrl(item.avatar);
        if (!item.storyExist) delete item.story;
        delete item.storyExist;
        return item;
      });
    } else {
      result = await User
        .find(query, projection)
        .limit(limit)
        .sort({ [sort[0]]: sort[1] });
      result = result.map(item => item.toObject({ virtuals: true, isAdmin: currentUserIsAdmin }));
    }
    result = await attachActivity(result);
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
};
