import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import User from '../../models/user';

export default async function hit(req) {
  req.checkBody('userId', 'Invalid user id').isMongoId();

  let result = null;

  try {
    await validateRequest(req);
    const userId = req.body.userId;

    const data = await User.findOneAndUpdate(
      { _id: userId },
      { $inc: { hit: 1 } },
      { new: true }
    ).select('hit incHit');

    if (!data) throw new HttpError(404);

    result = data.toObject({
      virtuals:  true,
      transform: (doc, ret) => {
        delete ret.avatarUrl;
        delete ret.hit;
        delete ret.incHit;
        return ret;
      }
    });
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
