import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import User from '../../models/user';
import RBAC from '../../utils/rbac';

export default async function confirm(req) {
  req.checkBody('token').isHexadecimal();

  let result = { success: false };
  const acl = RBAC.getAcl();

  try {
    await validateRequest(req);
    const { token } = req.body;

    let user = await User.findOne({ confirmToken: token }).select('+confirmToken +confirmExpire');
    if (!user) throw new HttpError(404);

    if (user.confirmExpire > Date.now()) {
      user.confirmToken = '';
      user.status = 'active';
      user = (await user.save()).toObject();
      await acl.asyncRemoveUserRoles(user.id, ['inactive']);
      await acl.asyncAddUserRoles(user.id, 'user');

      result = { success: true };

      if (req.session && req.session.user) {
        req.session.user.roles = await acl.asyncUserRoles(user.id);
        result.roles = req.session.user.roles;
      }
    } else {
      result = { success: false, errors: 'token expire' };
    }
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
