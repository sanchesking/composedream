import { formatErrors } from '../../utils/errors';
import Story from '../../models/story';
import User from '../../models/user';
import { validateRequest } from '../../utils/validate';

export default async function update(req) {
  req.checkBody('userId').isMongoId();
  req.checkBody('story.title').notEmpty();
  req.checkBody('story.story').notEmpty();

  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');
  let result = { errors: 'Can not update profile' };

  try {
    await validateRequest(req);

    const request = req.body;
    const authorId = request.userId;
    const hasPermission = (currentUserIsAdmin || (currentUser._id === authorId));

    if (!hasPermission) {
      req.res.status(403).end();
    }

    let story = await Story.findOne({ author: authorId });
    if (!story) {
      story = new Story();
    }

    story.author = authorId;
    story.story = request.story.story;
    story.title = request.story.title;

    const data = await story.save();
    await User.findByIdAndUpdate(authorId, { story: data._id });

    result = data.toObject({ virtuals: true });
  } catch (err) {
    return formatErrors(req, err);
  }
  return result;
}
