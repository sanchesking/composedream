import { HttpError, formatErrors } from '../../utils/errors';
import Story from '../../models/story';
import { validateRequest } from '../../utils/validate';

async function validateParams(req) {
  req.checkQuery('id').isMongoId();
  req.checkQuery('fields').optional().notEmpty();
  const validation = await req.getValidationResult();
  if (!validation.isEmpty()) {
    throw new HttpError(400, validation.array());
  }
  return true;
}

export default async function read(req) {
  req.checkQuery('id').isMongoId();
  req.checkQuery('fields').optional().notEmpty();

  let result = null;

  const currentUser = req.session.user;
  const currentUserIsAdmin = currentUser ? ~(currentUser.roles || []).indexOf('admin') : 0;

  try {
    await validateRequest(req);

    const fields = req.query.fields ? req.query.fields.split(',') : [];
    const withComments = Boolean(~fields.indexOf('comments'));

    let story = Story.findById(req.query.id);

    if (withComments) {
      story.populate('comments.author', 'name');
    } else {
      story.select('-comments');
    }
    story = await story.exec();

    if (!story) throw new HttpError(404);

    result = story.toObject({ virtuals: true, isAdmin: currentUserIsAdmin });
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
