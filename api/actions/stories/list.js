import { getSafeFields } from '../../utils/db';
import Story from '../../models/story';
import { formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';

function safeFields() {
  return {
    _id:        true,
    title:      true,
    story:      true,
    likesCount: true,
    totalLikes: true
  };
}

function getProjection(fields) {
  const safe = safeFields();
  if (~fields.indexOf('likesCount')) {
    safe.likes = true;
    safe.incLikes = true;
    fields.push('likes');
    fields.push('incLikes');
  }
  return getSafeFields(safe, fields);
}

export default async function list(req) {
  req.checkQuery('fields').notEmpty();
  req.checkQuery('sort').optional().matches(/^[a-zA-Z0-9_]+$/);
  req.checkQuery('limit').optional().isInt();

  let result = null;
  const currentUser = req.session.user;
  const currentUserIsAdmin = ~currentUser.roles.indexOf('admin');

  try {
    await validateRequest(req);

    const fields = req.query.fields.split(',');
    const limit = parseInt(req.query.limit, 10) || 100;
    const sort = req.query.sort ? req.query.sort.split('_') : ['name', 'asc'];
    sort[1] = sort[1] === 'desc' ? -1 : 1;

    const query = {};
    const projection = getProjection(fields);

    const data = await Story.find(query, projection).limit(limit).sort({ [sort[0]]: sort[1] });
    result = data.map(item => item.toObject({ virtuals: true, isAdmin: currentUserIsAdmin }));
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
};
