import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';
import Story from '../../models/story';

export default async function like(req) {
  req.checkBody('profileId').isMongoId();

  const profileId = req.body.profileId;
  const currentUserId = req.session.user._id;

  let result = null;

  const beforeSend = ({ likes, incLikes }) => likes.length + incLikes;

  try {
    await validateRequest(req);

    if (profileId === currentUserId) throw new HttpError(400, 'Can not like yourself');

    const data = await Story.findOne({ author: profileId }).populate('author', 'status');
    if (!data) throw new HttpError(404, 'Story not found');
    if (data.author && data.author.status === 'blocked' ) throw new HttpError(400, 'Author of the story is blocked');

    if (data.likes.indexOf(currentUserId) < 0) {
      data.likes.push(currentUserId);
    } else {
      data.likes = data.likes.filter( item => item.toString() !== currentUserId);
    }

    result = await data.save();
    if (!result) throw new HttpError(404, 'Can not update likes');

    result = {
      likesCount: beforeSend(result),
      totalLikes: result.totalLikes
    };
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
