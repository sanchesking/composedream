import Story from '../../models/story';
import { HttpError, formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';

export default async function update(req) {
  req.checkBody('userId').isMongoId();

  const currentUser = req.session.user;
  const currentUserIsAdmin = ~(currentUser.roles || []).indexOf('admin');
  let result = { errors: 'Can not update profile' };

  try {
    await validateRequest(req);

    const request = req.body;

    const story = await Story.findOne({ author: request.userId });
    if (!story) throw new HttpError(404);

    const hasPermission = (currentUserIsAdmin || (currentUser._id === story.author.toString()));
    if (!hasPermission) throw new HttpError(403);

    story.story = request.story.story;
    story.title = request.story.title;
    const data = await story.save();
    result = data.toObject({ virtuals: true });
  } catch (err) {
    return formatErrors(req, err);
  }
  return result;
}
