import cookie from 'react-cookie';
import { UserActivity } from '../models/index';

export default function logout(req) {
  return new Promise((resolve) => {
    if (req.session.user) {
      UserActivity.findOneAndUpdateStatusByUser(req.session.user._id, 'offline').exec();
    }

    req.session.destroy(() => {
      req.session = null;
      cookie.remove('composedream_AUTH_TOKEN');
      return resolve(null);
    });
  });
}
