export default function data(req) {
  const data = {
    footBall: [
      {
        title: 'junior soccer league 2016',
        imgUrl: 'junior',
        about: 'Hypnosis quit smoking methods maintain caused quite a stir in the medical world over the last two decades. There is a lot of argument pertaining to whether or not hypnosis quit smoking treatments are actually useful in the long_term. Nonetheless, there has been much research conducted concerning the treatments, and the agreement is that hypnosis treatments are just as effective as any variant stop smoking product. I am not discouraged, because every wrong attempt discarded is another step forward. Thomas Alva Edison',
        topBtns: [
          {caption: 'South Carolina', type: 'rounded'},
          {caption: 'South Carolina', type: 'rounded'}
        ],
        bottomBtns: [
          {caption: 'South Carolina', type: ''},
          {caption: 'South Carolina', type: 'disabled'},
          {caption: 'South Carolina', type: 'disabled'},
          {caption: 'South Carolina', type: 'disabled'},
          {caption: 'South Carolina', type: 'disabled'},
          {caption: 'South Carolina', type: ''}
        ],
        dates: [
          {start: '10 Sep 2016', end: '18 Oct 2016'},
          {start: '10 Sep 2016', end: '18 Oct 2016'}
        ]
      },
      {
        title: 'junior soccer league 2016',
        imgUrl: 'tourney',
        about: 'Hypnosis quit smoking methods maintain caused quite a stir in the medical world over the last two decades. There is a lot of argument pertaining to whether or not hypnosis quit smoking treatments are actually useful in the long_term. Nonetheless, there has been much research conducted concerning the treatments, and the agreement is that hypnosis treatments are just as effective as any variant stop smoking product. I am not discouraged, because every wrong attempt discarded is another step forward. Thomas Alva Edison',
        topBtns: [
          {caption: 'South Carolina', type: 'rounded'},
          {caption: 'South Carolina', type: 'rounded'}
        ],
        bottomBtns: [
          {caption: 'South Carolina', type: ''},
          {caption: 'South Carolina', type: ''},
          {caption: 'South Carolina', type: 'disabled'},
          {caption: 'South Carolina', type: 'disabled'},
          {caption: 'South Carolina', type: 'disabled'},
          {caption: 'South Carolina', type: ''}
        ],
        dates: [
          {start: '10 Sep 2016', end: '18 Oct 2016'},
          {start: '10 Sep 2016', end: '18 Oct 2016'}
        ]
      }
    ],
    fireBall: [
      {
        title: 'Fire Ball',
        subTitle: 'Dragon Ball',
        logo: 'shield_1',
        coach: 'Luke Blake',
        manager: 'Mario Young',
        record: '5 Matches Win in a Row',
        btns: [
          {caption: 'Tournament', type: 'cup'},
          {caption: 'U16 Boys', type: 'rounded'},
          {caption: 'West Darion', type: 'rounded'}
        ],
        players: [
          {photo: 'Tournament', name: 'Jay Northon', position: 'cup', ht: '6,3', wt: '174', homeTown: 'Lake Savannah', pass: false},
          {photo: 'Tournament', name: 'Jay Northon', position: 'cup', ht: '6,3', wt: '174', homeTown: 'Lake Savannah', pass: true},
          {photo: 'Tournament', name: 'Jay Northon', position: 'cup', ht: '6,3', wt: '174', homeTown: 'Lake Savannah', pass: false},
          {photo: 'Tournament', name: 'Jay Northon', position: 'cup', ht: '6,3', wt: '174', homeTown: 'Lake Savannah', pass: true},
          {photo: 'Tournament', name: 'Jay Northon', position: 'cup', ht: '6,3', wt: '174', homeTown: 'Lake Savannah', pass: false}
        ]
      }
    ],
    SoccerLeague: [
      {
        logo: 'shild.png',
        name: 'Junior soccer League 2016',
        date1: '10 Sep 2016 - 18 Oct 2016',
        date2: '10 Sep 2016 - 18 Oct 2016',
        description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque aut cum cupiditate debitis dolorum, numquam obcaecati omnis possimus similique voluptas? At blanditiis eligendi ex facilis minus natus quod, reiciendis repudiandae.',
        btns: [
          {
            type: 'teams',
            caption: 'U15 Boys'
          },
          {
            type: 'teams_a',
            caption: 'U15 Boys'
          },
          {
            type: 'teams',
            caption: 'U15 Boys'
          },
          {
            type: 'teams',
            caption: 'U15 Boys'
          }
        ],
        teamGroups: [
          {
            name: "Open",
            list: [
              {
                name: 'Blue Claws',
                logo: 'junior.png',
                rating: '2.2',
                manager: 'Some Guy',
                check: 'dis',
                money: 'error',
                waivers: 'ok'
              },
              {
                name: 'Blue Claws',
                logo: 'junior.png',
                rating: '2.2',
                manager: 'Some Guy',
                check: 'dis',
                money: 'error',
                waivers: 'ok'
              },
              {
                name: 'Blue Claws',
                logo: 'junior.png',
                rating: '2.2',
                manager: 'Some Guy',
                check: 'dis',
                money: 'error',
                waivers: 'ok'
              }
            ]
          },
          {
            name: "Closed",
            list: [
              {
                name: 'Blue Claws',
                logo: 'junior.png',
                rating: '2.2',
                manager: 'Some Guy',
                check: 'ok',
                money: 'ok',
                waivers: 'ok'
              },
              {
                name: 'Blue Claws',
                logo: 'junior.png',
                rating: '2.2',
                manager: 'Some Guy',
                check: 'error',
                money: 'error',
                waivers: 'error'
              },
              {
                name: 'Blue Claws',
                logo: 'junior.png',
                rating: '2.2',
                manager: 'Some Guy',
                check: 'dis',
                money: 'dis',
                waivers: 'dis'
              },
              {
                name: 'Blue Claws',
                logo: 'junior.png',
                rating: '2.2',
                manager: 'Some Guy',
                check: 'dis',
                money: 'error',
                waivers: 'ok'
              }
            ]
          }
        ]
      }
    ],
    Home: [
      {
        bigNews: {
          title: 'News',
          date: '20 Oct 2016',
          text: 'We invite you to attend the Junior soccer league 2016 in beautiful Rock Hill, SC! Registration is open! This event brings in teams from all over South Carolina, North Carolina, Georgia, Virginia, West Virginia, and Tennessee.'
        },
        slider: [
          {
            title: 'Tournament Time 2016',
            date: '20 Oct 2016 - 24 Oct 2016',
            logo: 'emblem_2.png'
          }
        ],
      }
    ]
  };
  return Promise.resolve(data);
}
