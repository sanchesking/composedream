import { ObjectId as objectId } from 'mongodb';
import Story from '../../models/story';
import { formatErrors } from '../../utils/errors';
import { validateRequest } from '../../utils/validate';

export default async function storyUpdate(req) {
  req.checkBody('userId').isMongoId();
  req.checkBody('count').isInt();

  let result = { errors: ['Can not update likes'] };

  const currentUser = req.session.user;
  const currentUserIsAdmin = ~currentUser.roles.indexOf('admin');

  const incCount = req.body.count;
  const userId = req.body.userId;

  try {
    await validateRequest(req);

    if (currentUser && currentUserIsAdmin) {
      const data = await Story.findOneAndUpdate(
        { author: objectId(userId) },
        { $set: { 'incLikes': incCount } },
        { new: true, projection: { 'likes': 1, 'incLikes': 1 } }
      );

      result = {
        likes:    data.likes.length,
        incLikes: data.incLikes
      };
    }
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
