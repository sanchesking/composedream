import { HttpError, formatErrors } from '../../utils/errors';
import Category from '../../models/category';
import { validateRequest } from '../../utils/validate';

export default async function read(req) {
  req.checkQuery('rootName').isAlpha();

  let result = null;

  try {
    await validateRequest(req);

    const { rootName } = req.query;

    const category = await Category.findOne({ name: rootName, parent: null }).populate('children', 'name');
    if (!category) throw new HttpError(404);

    result = category.toObject({ virtuals: true });
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
