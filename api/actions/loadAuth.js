import cookie from 'react-cookie';
import RBAC from '../utils/rbac';
import { SocialAuth } from '../models/index';

async function loadAuth(req, res) {
  cookie.plugToRequest(req, res);
  let result = null;

  if (req.session.user) {
    const auth = (await SocialAuth.findOne({ user: req.session.user._id }).populate('user', 'name email avatar status capital type hit incHit friends')).toObject();
    if (auth && auth.user) {
      result = auth.user;
      result.token = auth.token;
      result.roles = await RBAC.getAcl().asyncUserRoles(auth.user.id);
    }
  }

  return result;
}

export default loadAuth;
