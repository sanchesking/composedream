import User from '../../models/user';
import { validateRequest } from '../../utils/validate';
import { HttpError, formatErrors } from '../../utils/errors';

export default async function create(req) {
  req.checkBody('userId').isMongoId();

  let result = { success: false };
  const currentUser = req.session.user;

  try {
    await validateRequest(req);
    const { userId } = req.body;

    if (userId === currentUser._id.toString()) throw new HttpError(400, 'Can not add yourself to friends');

    let owner = await User.findOne({ _id: currentUser._id, friends: { $elemMatch: { $eq: userId } } });
    if (owner) throw new HttpError(400, 'You have already this user in friends');

    owner = await User.findById(currentUser._id);
    if (!owner) throw new HttpError(404, 'User not found');

    owner.friends.push(userId);
    await owner.save();

    result = { success: true };
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
