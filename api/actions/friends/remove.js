import User from '../../models/user';
import { validateRequest } from '../../utils/validate';
import { formatErrors } from '../../utils/errors';

export default async function create(req) {
  req.checkBody('userId').isMongoId();

  let result = { success: false };
  const currentUser = req.session.user;

  try {
    await validateRequest(req);
    const { userId } = req.body;

    let owner = await User.findById(currentUser._id);
    owner.friends.pull(userId);
    owner = await owner.save();

    result = { success: true };
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
