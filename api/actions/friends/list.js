import User from '../../models/user';
import Story from '../../models/story';
import { validateRequest } from '../../utils/validate';
import { formatErrors } from '../../utils/errors';

export default async function create(req) {
  req.checkQuery('userId').isMongoId();

  let result = { success: false };

  try {
    await validateRequest(req);
    const { userId } = req.query;

    const owner = await User.findById(userId).populate('friends', 'name location avatar hit incHit story');
    const storiesId = owner.friends.map(item => item.story);
    const storiesFriends = await Story.find({ _id: { $in: storiesId } }).select('author likes incLikes totalLikes');
    const storyFriends = storiesFriends.reduce((carry, item) => {
      const story = item.toObject();
      carry[story.author.toString()] = story;
      return carry;
    }, {});

    result = owner.friends.map(item => {
      const friend = item.toObject();
      if (friend.story) {
        friend.story = {};
        friend.story._id = storyFriends[friend.id]._id;
        friend.story.likesCount = storyFriends[friend.id].likesCount;
        friend.story.totalLikes = storyFriends[friend.id].totalLikes;
      }
      return friend;
    });
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
