import { UserActivity } from '../../models/index';
import { formatErrors } from '../../utils/errors';

export default async function online(req) {

  let result = { success: false };

  try {
    const { userIds } = req.body;

    const usersIsOnline = await UserActivity.findStatusByUser({ $in: userIds });

    result = usersIsOnline.reduce((carry, item) => {
      const doc = item.toObject();
      const userId = doc.user.toString();
      carry[userId] = doc.action;
      return carry;
    }, {});
  } catch (err) {
    return formatErrors(req, err);
  }

  return result;
}
