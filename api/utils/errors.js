function getHttpMessage(status) {
  switch (status) {
    case 400: return 'Bad request';
    case 403: return 'Forbidden';
    case 404: return 'Page not found';
    default: return 'Unknown http error';
  }
}

function HttpError(status, data) {
  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, this.constructor);
  } else {
    this.stack = (new Error()).stack;
  }

  this.name = this.constructor.name;
  this.message = getHttpMessage(status);
  this.status = status;
  this.data = {
    errors: data ? data : [this.message]
  };
}

function formatErrors(req, err) {
  if (err instanceof HttpError) {
    req.res.status(err.status);
    return err.data;
  }

  console.log('ERR=', err);

  req.res.status(500);
  return err;
}

module.exports = {
  formatErrors,
  HttpError
};

require('util').inherits(HttpError, Error);
