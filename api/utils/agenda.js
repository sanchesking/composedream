import Agenda from 'agenda';
import config from 'config';

const agenda = new Agenda({ db: { address: config.db.connect } });

const jobTypes = process.env.JOB_TYPES ? process.env.JOB_TYPES.split(',') : [config.jobs.default];

jobTypes.forEach(function(type) {
  require('../jobs/' + type)(agenda);
});

if (jobTypes.length) {
  agenda.on('ready', function() {
    agenda.start();
  });
}

export default agenda;
