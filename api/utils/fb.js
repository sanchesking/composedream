import Q from 'q';
import FB from 'fb';

export function fbApiAsync(query) {
  const deferred = Q.defer();
  Q.when(FB.api(query, function(res) {
    deferred.resolve(res);
  }));
  return deferred.promise;
}