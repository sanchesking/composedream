import mongoose from 'mongoose';
import Q from 'q';
import config from 'config';
import Category from '../models/category';
import { deepValue } from './object';
import agenda from '../utils/agenda';

export function connect(callback) {
  mongoose.Promise = Q.Promise;

  // mongoose.set('debug', true);
  mongoose.connect(config.db.connect, (error) => {
    if (error) {
      console.error('Please make sure Mongodb is installed and running!'); // eslint-disable-line no-console
      throw error;
    }

    agenda.on('ready', function() {
      agenda.every('*/5 * * * * *', 'update month likes');
    });

    Category.initCategory(Category);
    if (callback) callback();
  });
}

export function getSafeFields(safe, fields) {
  return fields.reduce((carry, field) => {
    const value = deepValue(safe, field);
    if (value && typeof value !== 'object') carry[field] = 1;
    return carry;
  }, {});
}
