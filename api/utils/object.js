/**
 * Return value by string "obj.foo.bar.baz"
 * @param obj
 * @param path
 * @returns {*}
 */
export function deepValue(obj, path) {
  for (var i = 0, path = path.split('.'), len = path.length; i < len; i++) {
    obj = obj[path[i]];
  }
  return obj;
}
