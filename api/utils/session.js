export function convertToSessionUser(user, roles) {
  const userId = user._id.toString();
  return {
    _id:     userId,
    roles:   roles,
    isAdmin: Boolean(~(roles).indexOf('admin'))
  };
}
