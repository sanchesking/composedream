import Q from 'q';
import path from 'path';
import nodemailer from 'nodemailer';
import emailTemplates from 'email-templates';
import stubTransport from 'nodemailer-stub-transport';
import { absoluteClientUrl } from '../utils/url';
import config from 'config';

const Mailer = {
  _transport: null,
  _template:  null,

  // init mailer component
  init: function(initConfig) {
    const deferred = Q.defer();
    const basePath = path.resolve(path.join(__dirname, '..'));

    // init template engine
    emailTemplates(basePath + initConfig.emailTplsDir, function(err, template) {
      if (err) {
        console.error(err);
        return deferred.reject(err);
      }

      this._template = template;
      // init mailer
      this._transport = nodemailer.createTransport(initConfig.transport === 'stub' ? stubTransport() : initConfig.transport);
      deferred.resolve();
    }.bind(this));

    return deferred.promise;
  },

  // отправка обычного e-mail
  sendMail: function(from, to, subject, text, html) {
    const deferred = Q.defer();
    const params = {
      from:    from, // sender address
      to:      to, // recipients list separated by commas
      subject: subject,
      text:    text
    };

    if (html) {
      params.html = html;
    }

    this._transport.sendMail(params, function(err, res) {
      if (err) {
        console.error(err);
        deferred.reject(err);
      } else {
        deferred.resolve(res);
      }
    });

    return deferred.promise;
  },

  // send mail with template
  sendMailTemplate: function(from, to, subject, tplName, locals) {
    const deferred = Q.defer();

    this._template(tplName, locals, function(err, html, text) {
      if (err) {
        console.error(err);
        return deferred.reject(err);
      }

      this.sendMail(from, to, subject, text, html)
        .then(function(res) {
          deferred.resolve(res);
        });
    }.bind(this));

    return deferred.promise;
  }
};

export async function sendConfirmationLink(from, to, data) {
  const subject = 'Confirmation email';
  const tplName = 'confirmEmail';
  const locals = {
    user:        data,
    confirmLink: absoluteClientUrl('/confirm-email', { token: data.confirmToken }),
  };
  await Mailer.init(config.mailer);
  const mail = await Mailer.sendMailTemplate(from, to, subject, tplName, locals);
  return mail;
}

export default Mailer;
