import { HttpError } from './errors';

export async function validateRequest(req) {
  const validation = await req.getValidationResult();
  if (!validation.isEmpty()) {
    throw new HttpError(400, validation.array());
  }
  return true;
}
