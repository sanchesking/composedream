import Q from 'q';
import mkdirp from 'mkdirp';
import fs from 'fs';
import path from 'path';
import { absoluteUrl } from '../utils/url';

export const staticBasePath = path.join(__dirname, '..', 'public');
export const staticBaseUrl = '/static';

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

export function generateRandomString(length) {
  const characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  const charactersLength = characters.length;
  let randomString = '';
  for (let i = 0; i < length; i++) {
    randomString = randomString + characters[getRandomInt(0, charactersLength - 1)];
  }
  return randomString;
}

export function writeBase64File(strFile, relativePath, fileName) {
  const deferred = Q.defer();
  const filePath = path.join(staticBasePath, relativePath);
  let error = false;
  const reg = new RegExp('^data:image\/(png|jpeg|jpg|gif);base64,');
  const matchesExt = strFile.match(reg);

  const ext = Array.isArray(matchesExt) ? matchesExt[1] : false;
  const base64Data = strFile.replace(reg, '');

  const newFileName = fileName ? fileName : generateRandomString(10);

  if (!ext) {
    error = true;
    deferred.reject('Incorrect file type');
  }

  if (!error) {
    mkdirp(filePath, function(e) {
      if (e) {
        deferred.reject(e);
      }

      fs.writeFile(filePath + `/${newFileName}.${ext}`, base64Data, 'base64', function(err) {
        if (err) {
          deferred.reject(err);
        } else {
          deferred.resolve(staticBaseUrl + relativePath + `/${newFileName}.${ext}`);
        }
      });
    });
  }

  return deferred.promise;
}

export function writeFile(file, relativePath, name) {
  const deferred = Q.defer();
  const { filename } = file;
  const filePath = path.join(staticBasePath, relativePath);
  const fileName = name ? name + path.extname(filename) : filename;

  mkdirp(filePath, function(err) {
    if (err) {
      deferred.reject(err);
    }
    file.pipe(fs.createWriteStream(filePath + '/' + fileName));
    deferred.resolve(staticBaseUrl + relativePath + '/' + fileName);
  });
  return deferred.promise;
}

export function deleteFile(relativePath) {
  const deferred = Q.defer();
  const modRelPath = relativePath.replace(/^\/static\//, '');
  fs.unlink(path.join(staticBasePath, modRelPath), (err) => {
    if (err) {
      deferred.resolve(false);
    } else {
      deferred.resolve(true);
    }
  });

  return deferred.promise;
}

export function getImageUrl(imagePath) {
  return absoluteUrl(imagePath ? imagePath : '/static/no-image.png');
}
