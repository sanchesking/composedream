import _ from 'underscore';
import cookie from 'react-cookie';
import { User, UserActivity } from '../models/index';
import SocialAuth from '../models/socialAuth';
import RBAC, { initUserRoles } from '../utils/rbac';
import { convertToSessionUser } from '../utils/session';

async function setOnline(userId) {
  const status = await UserActivity.findOneAndUpdateStatusByUser(userId, 'online');
  return status;
}

export default function() {
  return async function authMiddleware(req, res, next) {
    const acl = RBAC.getAcl();
    cookie.plugToRequest(req, res);
    const token = cookie.load('composedream_AUTH_TOKEN');
    let userId = '*';

    try {
      if (req.session.user) {
        userId = req.session.user._id.toString();
        await setOnline(userId);
      } else {
        if (token) {
          const auth = await SocialAuth.findOne({ token: { $exists: true, $eq: token, $ne: null } }).populate('user');
          if (auth && auth.user) {
            userId = auth.user.id;
            req.session.user = convertToSessionUser(auth.user, await acl.asyncUserRoles(userId));
            await setOnline(userId);
          } else {
            userId = '*';
          }
        } else {
          userId = '*';
        }
      }

      const routeParts = req.path;

      const roles = await acl.asyncUserRoles(userId);
      if (req.session.user && _.isEmpty(roles)) {
        const user = await User.findById(req.session.user._id);
        let email = null;
        if (user) {
          email = user.email
        }
        await initUserRoles(userId, email);
      }

      acl.isAllowed(userId, routeParts, req.method, (error, isAllowed) => {
        // console.log('---------------------------------');
        // console.log('USERID ', userId, 'ROLES', roles, ' PATH ', routeParts, ' ALLOW ', isAllowed);
        if (isAllowed) {
          next();
        } else {
          res.status(401).end();
        }
      });
    } catch (err) {
      console.log('ERR', err);
      res.status(500).end();
    }
  };
}
