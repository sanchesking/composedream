import { UserActivity } from '../models/index';

export default function login(socket, activity) {
  socket.on('login', (data) => {
    if (!activity.users[data.userId]) {
      activity.users[data.userId] = [socket.id];
    } else {
      activity.users[data.userId].push(socket.id);
    }
    activity.sockets[socket.id] = data.userId;

    if (data.userId) {
      UserActivity.findOneAndUpdateStatusByUser(data.userId, 'online');
    }
  });
}