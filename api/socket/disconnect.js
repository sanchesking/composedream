import _ from 'underscore';
import { UserActivity } from '../models/index';

export default function(socket, activity) {
  socket.on('disconnect', () => {
    const userId = activity.sockets[socket.id];
    delete activity.sockets[socket.id];

    if (!activity.users[userId]) return;

    activity.users[userId] = activity.users[userId].filter(item => item !== socket.id);

    if (_.isEmpty(activity.users[userId])) {
      UserActivity.findOneAndUpdateStatusByUser(userId, 'offline').exec();
    }
  });
}