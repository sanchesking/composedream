import * as actions from '../actions/index';

export default function online(socket) {
  socket.on('online', async (data) => {
    const res = await actions.userActivities.online({
      body: data
    });
    socket.emit('online2', res);
  });
}
