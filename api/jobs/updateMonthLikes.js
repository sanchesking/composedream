import Story from '../models/story';

module.exports = function(agenda) {
  agenda.define('update month likes', async function(job, done) {
    try {
      // console.log('log::start::update month likes');
      const date = new Date();
      const story = await Story.findOne({ $or: [
        { likesUpdateDate: { $lt: date.getTime() } },
        { likesUpdateDate: { $exists: false } }
      ] });
      if (story) {
        const firstDayNextMonth = new Date(date.getFullYear(), date.getMonth() + 1, 1);

        story.totalLikes = story.totalLikes + story.likes.length + story.incLikes;
        story.incLikes = 0;
        story.likes = [];
        story.likesUpdateDate = firstDayNextMonth.getTime();
        await story.save();
        done();
        // console.log('log::success::update month likes');
      } else {
        done();
        // console.log('log::finish::update month likes');
      }
    } catch (err) {
      done(err);
    }
  });
};
